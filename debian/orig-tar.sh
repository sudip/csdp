#!/bin/sh

# called by uscan with '--upstream-version' <version> <file>
tar xzf $3
rm -f $3
rm -f Csdp-*/doc/*.bbl Csdp-*/doc/*.blg Csdp-*/doc/*.log 
rm -f Csdp-*/doc/*.aux Csdp-*/doc/csdpuser.pdf doc/example.c
tar czf $3 Csdp-*
rm -rf Csdp-*

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $3 $origDir
  echo "moved $3 to $origDir"
fi

exit 0
